'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
Molly Rodda's Portfolio for ME 305- Introduction to Mechatronics
Cal Poly SLO-Winter 2021

@page page_fibonacci Fibonacci

@section page_fib_src Source Code Access
You can find the source code for the fibonacci program here:
https://bitbucket.org/dnguy259/me-305/src/master/Lab1/fibonacci.py

@section page_fib_doc Documentation
The documentation for the fibonacci program is located at fibonacci.fib




@page page_elevator_task Elevator Task-Finite State Machine

@section page_elevator_task_src Source Code Access
You can find the source code for Elevator Task here:
https://bitbucket.org/dnguy259/me-305/src/master/HW0/elevator_task.py


@section page_elevator_task Images:
    @image html HW0.PNG



@page page_TaskBlink Blinking LED Task

@section page_TaskBlink_src Source Code Access:
    You can find the source code for Task Blink Access here:
        https://bitbucket.org/dnguy259/me-305/src/master/Lab2/TaskBlink.py
    

@section page_TaskBlink_Images Images:
    @image html TaskBlink_Diagram.PNG

@section page_TaskBlink Demonstrating Video:
    You can see the video of LED blinking demonstration here:
        https://drive.google.com/file/d/1iLcH_CrsBLOrCiMevuy-9RDIVho7AEw-/view?usp=sharing


@page page_TaskGame LED Blinking Game

@section page_TaskGame_src Source Code Access:
    You can find the source code for LED Blinking Game here:
        https://bitbucket.org/dnguy259/me-305/src/master/Lab%203/TaskGame.py
        
@section page_TaskGame Demonstrating Video: 
    You can see the video of the game demonstration here:
        https://drive.google.com/file/d/1-MiKcEt3YTgdIITcZSqD1N73b9cWGCEl/view?usp=sharing


'''