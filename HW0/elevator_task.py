
'''@file: elevator_task.py
@Description: A file containing a class represeting a finite state machine- Elevator
@Author: Molly Rodda
@Date: Jan 27th, 2021
'''

class elevator_task:
    ''' This is a task list for an elevator, implementing a finite state machine'''

    ## Define Initial State
    def __init__(self):
        ''' Elevator Tasks under different actions:
            State 1: Moving Down
            State 3: Stop at bottom floor
            State 2: Moving up as button 2 is pressed
            State 4: Stop at top floor
        '''
       
        ## Represents state of motor. 0 for stationary; 1 for moving up, 2 for moving down
        self.motor = 2
        
        ## Represents state of the first floor button
        self.button_1 = 0
        
        ## Represents state of the second floor button
        self.button_2 = 0
        
        ## Represents state of the finite state machine, 1 for first floor, 2 for top floor 
        self.state = 1
        
    ## Define State 0: Moving down button_1 = 0; motor = 0 (Not Moving); state = 1(first floor);              
    def set_first(self):
        if self.state ==1: ## Elevator is at STATE 1
           self.button_1 = 0 ## Clear Button 1
           self.motor = 0 ## Motor stops at first floor
           self.state =3 ## Elevator is now at bottom floor-STATE 3
           
    def set_button_2(self):
        if self.state ==3: ## Elevator is at STATE 3
           self.button_2 = 1 ##Button 2 is pressed
           self.motor = 1 ## Moving UP
           self.state =2 ## Elevator is now moving up-STATE 2
    
    def set_second(self):
        if self.state ==2: ## Elevator is at STATE 2
           self.button_2 = 0 ## Button 2 is cleared
           self.motor = 0 ## Motor STOP
           self.state = 4 ## Elevator stops at top floor- STATE 4
        
    def set_button_1(self):
        if self.state == 4:
           self.button_1 = 1 ## Button 1 is pressed
           self.motor = 2 ## Moving down
           self.state = 1 ## Elevator is moving up-STATE 1
           
            