import pyb
import utime
import random
PRESS = False

#Create Pin Object that controls LED 
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

#Create Pin object controls Button B1
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
#Define PRESSED_Callback function

def PRESSED_Callback(IRQ_src):
    global PRESS 
    PRESS = not PRESS
    
    
        
    
    
class Game:

    def __init__(self):

    #Print Welcome message
        print('Welcome! See the LED blinking pattern, then re-do the pattern')
        print('Note: Button pressed = LED ON, Button released = LED OFF')
        #Callback function to measure FALLING/RISING edge
        pyb.ExtInt(pinC13, mode =pyb.ExtInt.IRQ_RISING_FALLING, pull= pyb.Pin.PULL_NONE, callback = PRESSED_Callback)

        while True:    
            print('START!')
            pyb.delay(5000) #Give user sometime to be ready
            PlayerLost = False
    #Create List of patterns
            choice = [[1,1,1],[1,1,1,1,1]] #Delta Time LED in pattern stayed ON
            pattern = random.choice(choice) #Choose randomly a pattern in choice


    #Blink the LED according to the chosen pattern
            for timeON in pattern:
                pinA5.high()
                start = utime.ticks_ms() #Capture start time
            
                while True:
                    curr_time = utime.ticks_ms() #Capture current time
                    difference = utime.ticks_diff(curr_time, start) / 1000 #find time elapsed(in second) since start time from line 49
                    if difference > timeON: # if time elapsed runs passed timeON 
                        pinA5.low() #turn off LED
                        break
    
    # wait 1 second before turning it back on
                start = utime.ticks_ms()
                while True:
                    curr_time = utime.ticks_ms()
                    difference = utime.ticks_diff(curr_time, start) / 1000
                    if difference >= 1:
                        break

            print('Your Turn!')
    

   
    
    #Compare user's input with given pattern:
            for timeON in pattern:
                pinA5.low()
                #print('LED OFF')
                global PRESS
                while not PRESS: #while loop for when PRESS = False
                    #print('Line 68')
                    pass
                    #Stay in the while loop in Line 70 until Press = True
            
                Pressed_Time = utime.ticks_ms() #Record the time the button is pressed
                pinA5.high()
                #print('LED ON')
                while PRESS: #while loop for when PRESS = True
                    #print("Button is pressed")
                    pass
                    #stay in the while loop in Line 75 untill Press = False
            
                pinA5.low()
                #print('LED off')
                Released_Time = utime.ticks_ms()
            
            
                #Calculate the actual time (in second) the player has LED ON:
                Player_Input_timeON = utime.ticks_diff(Released_Time,Pressed_Time)/1000    
                Player_timeON = round(Player_Input_timeON)
            
                if Player_timeON != timeON:
                    print("Game Over")
                    PlayerLost= True
                    break
            if PlayerLost:
                break
            

SimonSays= Game()


                        
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    