## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Molly Rodda's Portfolio for ME 305- Introduction to Mechatronics
#  Cal Poly SLO-Winter 2021
#
#  @section sec_lab1 Fibonacci Sequence
#  This is a program that can calculate Fibonacci numbers. Please see https://bitbucket.org/dnguy259/me-305/src/master/Lab1/fibonacci.py
#
# 
#
#  @author Molly Rodda
#
#  
#  @date 1/15/2021
#