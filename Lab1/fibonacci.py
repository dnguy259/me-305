## @file fibonacci.py
#  Brief doc for fibonacci.py
#
#  Detailed doc for fibonacci.py 
#
#  @author Molly Rodda
#
#  @date Jan 14, 2021
#
#  @package fibonacci
#
#
#  Detailed doc for the fibonacci module
#
#  @author Molly Rodda
#

#  @date Jan 14, 2021

## @brief Get fibonacci number
#
# @param n
# Define the first two values in the sequence
# n0 is the first index

def fib (n):
    
    f0= 0
    f1= 1
    count = 2


    if n < 0:
        fth = 'Invalid Input'
            
    elif n == 0:
        fth = f0
    
    elif n == 1:
        fth = f1
        
    else:
       while count <= n:
           fth = f0 + f1
           #update the seed numbers:
           f0 = f1
           f1 = fth
           count += 1
    return fth

if __name__=='__main__':
    run = 0
    while run <=5:
        try:
            idx = int(input('Please enter a positive number to start :'))
            fth = fib(idx)
            print ('Fibonacci number at n = {} is {}'.format (idx,fth))
            run +=1
        
        except ValueError:
            print('Invalid input')
            run +=1