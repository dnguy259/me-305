import utime
import pyb
import math

Button_Press = False
print("Ready!")

def Button_Callback(IRQ_src):
    global Button_Press
    Button_Press =True


class TaskBlink:
    
    def __init__(self):
        #Create Pin Object that control by Button B1
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        #Create Pin Object that control the LED
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
        self.tim2  = pyb.Timer(2, freq=20000)
        self.t2ch1 = self.tim2.channel(1,pyb.Timer.PWM, pin = self.pinA5)
    
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = Button_Callback)
        self.state = -1

        while True:
            global Button_Press
            if Button_Press ==True: #If the Button is pressed
                self.state = (self.state + 1) % 3 #Change to the next state since button is pressed
                if self.state == 0: 
                    print ("Square Wave")
                elif self.state == 1:
                    print ("Sine Wave")
                elif self.state == 2:
                    print("Saw Wave")
                   
                    
                self.start = utime.ticks_ms() #Start time at the begin of state
                Button_Press = False #Clear Button back to unpressed state
            
            if self.state ==0:#Square Wave
                current_time = utime.ticks_diff(self.start, utime.ticks_ms())%1000
                if current_time <500:
                   self.t2ch1.pulse_width_percent(100)
                   print('LED on')
                else:
                    self.t2ch1.pulse_width_percent(0)
                    print('LED off')
                    
            if self.state ==1: #Sine wave
               current_time = utime.ticks_diff(utime.ticks_ms(),self.start)%10000
               brightness = 50 * math.sin(0.0002 * math.pi * current_time) + 50
               print('Current brightness {}, current_time {}'.format(brightness, current_time))
               self.t2ch1.pulse_width_percent(brightness)
           
                   
                       
            if self.state ==2: #Saw Wave
               current_time = utime.ticks_diff(utime.ticks_ms(),self.start)%1000
               brightness = 0.1*current_time
               self.t2ch1.pulse_width_percent(brightness)
               print('Brightness is {}, current time is {}'.format(brightness, current_time))
               
                           
                           
Test = TaskBlink()

                    
                
                
    
 