'''
@file           elevator_task.py
@brief          A file containing a class representing an elevator.
@details        Represents the finite state machine.
@author         Molly Rodda
@copyright      This file is licensed under the creative commons CC-BY-NC-SA 4.0
'''

class ElevatorTask:
    '''
    @brief A class representing an elevator
    '''

    def __init__(self):
        '''
        @brief Create an ElevatorTask
        '''
        ## @brief This represents the state of the motor. It starts moving down.
        self.motor = 2
        ## @brief This represents the state of the first floor button.
        self.button_1 = 0
        ## @brief This represents the state of the second floor button.
        self.button_2 = 0
        ## @brief This represents the state of the finite state machine.
        self.state = 1

    def set_first(self):
        '''
        @brief Turn the input 'first' on.
        '''
        if self.state == 1:
            self.button_1 = 0
            self.motor = 0
            self.state = 3

    def set_button_2(self):
        '''
        @brief Press button_2
        '''
        if self.state == 3:
            self.button_2 = 1
            self.motor = 1
            self.state = 2

    def set_second(self):
        '''
        @brief Turn the input 'second' on.
        '''
        if self.state == 2:
            self.button_2 = 0
            self.motor = 0
            self.state = 4

    def set_button_1(self):
        '''
        @brief Press button_1
        '''
        if self.state == 4:
            self.button_1 = 1
            self.motor = 2
            self.state = 1

if __name__ == '__main__':
    elevator_1 = ElevatorTask()
    elevator_2 = ElevatorTask()

    elevator_1.set_first()
    elevator_2.set_first()

    elevator_1.set_button_2()
    elevator_1.set_second()
    elevator_1.set_button_1()
    elevator_1.set_first()

