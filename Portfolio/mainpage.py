'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
Molly Rodda's Portfolio for ME 305- Introduction to Mechatronics
Cal Poly SLO-Fall 2020


@page page_fibonacci Fibonacci

@section page_fib_src Source Code Access
You can find the source code for the fibonacci program here:
https://bitbucket.org/dnguy259/me-305/src/master/Lab1/fibonacci.py

@section page_fib_doc Documentation
The documentation for the fibonacci program is located at fibonacci.fib


@page page_elevator_task Elevator Task

@section page_elevator_taks_src Source Code Access
You can find the source code for Elevator Task here:
https://bitbucket.org/dnguy259/me-305/src/master/HW0/elevator_task.py


@section page_elevator_task_doc Document
The documentation for the Elevator Task program is located at elevator_task.ElevatorTask]

@section page_elevator_task Images
@image html state_diagram.png


@page page_FSM Finite State Machine Multi-Tasks

@section page_FSM_src Source Code Access
You can find the source code for Task.py here: 
https://bitbucket.org/dnguy259/me-305/src/master/Lab002b/Task.py
You can find the source code for main.py here:
https://bitbucket.org/dnguy259/me-305/src/master/Lab002b/main.py

@section page_FSM_doc Documentation
The documentation for the Finite State Machine Multi-Tasks is located at main.py    
    
'''