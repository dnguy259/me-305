"""
@file Task.py

@author: Molly Rodda
"""

import time
class TaskBlink:
    S0 = 0
    S1 = 1
    S2 = 2
    
    def __init__(self):
        self.state= self.S0
        self.interval = 1
        self.curr_time = time.time()
        self.next_time = self.curr_time + self.interval
    
    def run(self):
        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            if self.state == self.S0:
                self.state = self.S1
            elif self.state == self.S1:
                self.state = self.S2
                print('LED ON')
            elif self.state == self.S2:
                self.state = self.S1
                print('LED OFF')
                
            self.next_time = self.curr_time + self.interval
            
            
class TaskPattern:
    import pyb
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq=20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    def __init__(self):
        self.state = 0
        self.interval = 1
        self.curr_time = time.time()
        self.next_time = self.curr_time + self.interval
        
    def run(self):
        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            if self.state == 0:
               self.state = 1
            
            elif self.state == 1:
                 self.state = 2
                 self.t2ch1.pulse_width_percent(0)
                
            elif self.state == 2:
                self.state = 3
                self.t2ch1.pulse_width_percent(25)
                
            elif self.state == 3:
                self.state = 4
                self.t2ch1.pulse_width_percent(50)
            
            elif self.state == 4:
                self.state = 5
                self.t2ch1.pulse_width_percent(75)
                
            elif self.state == 5:
                self.state = 1
                self.t2ch1.pulse_width_percent(100)
            
            self.next_time = self.curr_time + self.interval