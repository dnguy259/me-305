"""
@file main.py

@author: Molly Rodda
"""

from Task import TaskBlink
from Task import TaskPattern

task1= TaskBlink()
task2= TaskPattern()

for n in range (100000):
    task1.run()
    task2.run()
