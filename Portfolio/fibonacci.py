## @file fibonacci.py
#  Brief doc for fibonacci.py
#
#  Detailed doc for fibonacci.py 
#
#  @author Molly Rodda
#
#  @date September 24, 2020
#
#  @package fibonacci
#
#
#  Detailed doc for the fibonacci module
#
#  @author Molly Rodda
#

#  @date September 24, 2020

## @brief Get fibonacci number
#
# @param n
def fib (n):
    print('Calculating Fibonacci number at '
          'index n = {:}.'.format(n))
    # Define the first two values in the sequence
    # n0 is the first index
    n0= 0
    n1= 1
    count =0
    #If user gives negative input
    if n <= 1:
        print("Invalid Input")
    #If user asks for the first value in Fibonacci sequence
    else:
        print ("Fibonacci sequence:")
        while count < n :
            print(n0)
            nth = n0 + n1
            #Updated values
            n0 = n1
            n1 = nth
            count += 1

if __name__=='__main__':
    n = int(input('Please enter a positive integer'))
    fib(n)
        
    

  